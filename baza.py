#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import platform
import pkg_resources
from sense_hat import SenseHat
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, g
from flask import render_template
from flask import send_from_directory
from flask import Flask, render_template, redirect, url_for
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm 
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Email, Length
from flask_sqlalchemy  import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user


app = Flask(__name__)
db1 = SQLAlchemy(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///login.db'

class User(UserMixin, db1.Model):
    id = db1.Column(db1.Integer, primary_key=True)
    username = db1.Column(db1.String(15), unique=True)
    email = db1.Column(db1.String(50), unique=True)
    password = db1.Column(db1.String(80))
